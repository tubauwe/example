(function(){
  angular.module('starter').controller('MainController', ['$scope', '$ionicModal', '$ionicPlatform', 'DatabaseService',  MainController]);

  function MainController($scope, $ionicModal, $ionicPlatform, databaseService) {
    var mc = this;

    // Initialize the database.
    $ionicPlatform.ready(function() {
      databaseService.initDB();

      // Get all data records from the database.
      databaseService.getAllData()
        .then(function(allData) {
          mc.allData = allData;
        });
    });

    // Initialize the modal view.
    $ionicModal.fromTemplateUrl('templates/add-data.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    mc.showAddDataModal = function() {
      $scope.data = {};
      $scope.modal.show();
    };

    $scope.saveData = function() {
      console.log('saveData()');
      $scope.data.type = 'one';
      databaseService.addData($scope.data);
      $scope.modal.hide();
    };

    return mc;
  }
})();
