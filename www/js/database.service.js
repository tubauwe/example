(function(){
  angular.module('starter').factory('DatabaseService', ['$q', DatabaseService]);

  function DatabaseService($q) {
    var _db;
    var _allData;

    return {
      initDB: initDB,
      addData: addData,
      getAllData: getAllData
    };

    function initDB() {
      if(!_db) {
        _db = new PouchDB('exampledb', { adapter: 'websql'});
        if (!_db.adapter) {
          _db = new PouchDB('exampledb');
        }
        // Turn on encryption.
        return _db.crypto('pa$$w0rd')
          .then(function(){
            return _db;
          });
      };
    };

    function addData(data) {
      return $q.when(_db.post(data));
    }

    function mapAllData(doc) {
      if (doc.type === 'one') {
        emit(doc._id);
      }
    }

    function getAllData() {
      if (!_allData) {
        return $q.when(_db.query(mapAllData, { include_docs: true}))
          .then(function(docs) {
            _allData = docs.rows.map(function(row) {
              return row.doc;
            });
            _db.changes({ live: true, since: 'now', include_docs: true})
              .on('change', onDatabaseChange);
            return _allData;
          })
      } else {
        return $q.when(_allData);
      }
    }

    function onDatabaseChange(change) {
      var index = findIndex(_allData, change.id);
      var data = _allData[index];

      if (change.deleted) {
        if (data) {
          _allData.splice(index, 1); // delete
        }
      } else {
        if (data && data._id === change.id) {
          _allData[index] = change.doc; // update
        } else {
          _allData.splice(index, 0, change.doc) // insert
        }
      }
    }

    function findIndex(array, id) {
      var low = 0, high = array.length, mid;
      while (low < high) {
        mid = (low + high) >>> 1;
        array[mid]._id < id ? low = mid + 1 : high = mid
      }
      return low;
    }

  }
})();
